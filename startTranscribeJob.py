'''
AWS Lambda Requirements:
* Python 3.7
* Environment variables:
    - BUCKET_NAME [REQUIRED]
    - KEY (second can be empty) to the file where SES stores files
    - VOCABULARY_NAME: existing vocabulary that will be used for transcription job
    - TRANSCRIBE_NAME_UID [REQUIRED]: string that will be added to the beginning of the transcription job name
* IAM Lambda role should allow "transcribe:StartTranscriptionJob" calls and ReadWrite access to S3
* Timeout of Lambda should be 1 minute
'''

import traceback
import boto3
import email
import os
import re

# Environment Variables
BUCKET_NAME = os.environ['BUCKET_NAME']
KEY = os.environ['KEY'] if 'KEY' in os.environ else ''
VOCABULARY_NAME = os.environ['VOCABULARY_NAME'] if 'VOCABULARY_NAME' in os.environ else None
TRANSCRIBE_NAME_UID = os.environ['TRANSCRIBE_NAME_UID']

# Global variables
AWS_TRANCSRIBE_PATTERN = '^[0-9a-zA-Z._-]*$' # Pattern that Transcription Job Name should satisfy


def lambda_handler(event, context):
    '''
    Main function that starts Transcription job
    '''
    try:
        # Check environment variables compliance
        if not TRANSCRIBE_NAME_UID:
            print('ERROR Environment variable TRANSCRIBE_NAME_UID cannot be empty. '
                  'Please add the value that satisfies regular expression pattern: ^[0-9a-zA-Z._-]')
            return 0
        else:
            # Check whether user input satisfies constraints
            if not re.compile(AWS_TRANCSRIBE_PATTERN).match(TRANSCRIBE_NAME_UID):
                print('ERROR Environment variable TRANSCRIBE_NAME_UID '
                      'must satisfy regular expression pattern: ^[0-9a-zA-Z._-]')
                return 0
        # Get message Id from event
        msg_id = event['Records'][0]['ses']['mail']['messageId']
        # Download MIME email from S3
        try:
            s3 = boto3.resource('s3')
            download_email = s3.Object(BUCKET_NAME, f'{KEY}{msg_id}').download_file(
                f'/tmp/{msg_id}')
        except Exception as e:
            print(f'ERROR while accessing S3 file: {e}')
            return 0
        else:
            # Get audio part
            msg_content = None
            with open(f'/tmp/{msg_id}', 'r') as f:
                msg_content = f.read()
            for part in email.message_from_string(msg_content).get_payload():
                if part.get_content_disposition() == 'attachment':
                    old_filename = part.get_filename()
                    print(f'INFO Found attachment: {old_filename}.')
                    # Change filename so it satisfies constraints of the Transcription Job Name
                    filename = ''
                    for letter in old_filename:
                        if re.compile(AWS_TRANCSRIBE_PATTERN).match(letter):
                            filename += letter
                    try:
                        # Writing file to S3
                        s3 = boto3.resource('s3')
                        put_audio_to_s3 = s3.Bucket(BUCKET_NAME).put_object(
                            ACL='aws-exec-read',
                            Body=part.get_payload(decode=True),
                            Key=f'audio/{msg_id}-{filename}')
                    except Exception as e:
                        print(f'Unable to upload audio file to the S3: {e}')
                        return 0
                    else:
                        # Start transcription job
                        transcribe = boto3.client('transcribe')
                        # Cut the name if it is too long
                        transcription_job_name = f'{TRANSCRIBE_NAME_UID}{msg_id}-{filename}'[:200]
                        print(f'INFO Transcription Job Name: {transcription_job_name}')
                        if VOCABULARY_NAME:
                            start_transcription_job = transcribe.start_transcription_job(
                                TranscriptionJobName=transcription_job_name,
                                LanguageCode='en-US',
                                MediaFormat='wav',
                                Media={'MediaFileUri': f'https://s3.amazonaws.com/{BUCKET_NAME}/audio/{msg_id}-{filename}'},
                                OutputBucketName=f'{BUCKET_NAME}',
                                Settings={
                                    'VocabularyName': VOCABULARY_NAME,
                                    'ChannelIdentification': False
                                }
                            )
                        else:
                            start_transcription_job = transcribe.start_transcription_job(
                                TranscriptionJobName=transcription_job_name,
                                LanguageCode='en-US',
                                MediaFormat='wav',
                                Media={'MediaFileUri': f'https://s3.amazonaws.com/{BUCKET_NAME}/audio/{msg_id}-{filename}'},
                                OutputBucketName=f'{BUCKET_NAME}'
                            )
    except Exception:
        print('ERROR Some error occured. Please refer to the traceback below.')
        traceback.print_exc()
        return 0