'''
Prerequisites:
* CloudWatch Event Rule
    * Event Pattern
    * Service Name: Transcribe
    * Event Type: Transcribe Job State Change
    * Specific statuses: COMPLETED, FAILED
    * Targets: Lambda function and add function name from list. Leave everything else default
* AWS Lambda configuration
    * Python3.7
    * Timeout 1 min
    * Memory 512 MB (Recommend to add as much memory as the biggest expected audio file is plus a bit)
    * Environment variables
        - BUCKET_NAME [REQUIRED]: the name of the S3 bucket where old emails, media and transcripts are stored.
         This script implies that everything is stored in the same S3 and WAV files are put with 'audio/' KEY.
        - KEY: Key to the emails in S3 (if root leave empty)
        - SOURCE_EMAIL [REQUIRED]: email address under your domain which will be used by SES to send an email
        - EMAIL_RECIPIENT [REQUIRED]: destination email address
        - EMAIL_SUBJECT: Subject of the outgoing email, by default is 'Voicemail Transcription Enclosed'
        - TRANSCRIBE_NAME_CONTAINS [REQUIRED]: string which should identify transcription jobs related
          to the Voicemail Transcription Task
        - ADMINISTRATIVE_CONTACT [REQUIRED]: Contact email where notification about failure will be sent
    * IAM Role with:
        1) Access to call "ses:SendRawEmail" and "SendEmail" operation
        2) readwrite S3 access to the S3 bucket you specified within "BUCKET_NAME" env variable
'''

import os
import re
import json
import boto3
import traceback
from email import policy
from email.parser import BytesParser
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart

# Environment variables
BUCKET_NAME = os.environ['BUCKET_NAME']
KEY = os.environ['KEY'] if 'KEY' in os.environ else ''
SOURCE_EMAIL = os.environ['SOURCE_EMAIL']
EMAIL_RECIPIENT = os.environ['EMAIL_RECIPIENT']
EMAIL_SUBJECT = os.environ['EMAIL_SUBJECT'] if 'EMAIL_SUBJECT' in os.environ else 'Voicemail Transcription Enclosed'
TRANSCRIBE_NAME_CONTAINS = os.environ['TRANSCRIBE_NAME_CONTAINS']
ADMINISTRATIVE_CONTACT = os.environ['ADMINISTRATIVE_CONTACT']


def form_email_message(subject, from_address, to_address, body, audio={}):
    '''
    Function to form an email in MIME format
    :param subject: Email subject
    :param from_address: source email
    :param to_address: destination email
    :param body: Email Body text
    :param audio_path: system path to the attachment file
    :param audio_name: filename of the attachment
    :param old_eml_path: system path to the old EML file
    :param old_eml_name: name of the EML attachment
    :return: MIMEMultipart message
    '''
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = from_address
    msg['To'] = to_address
    # Adding body to the message
    part = MIMEText(body)
    msg.attach(part)
    # Adding audio to the message
    if audio:
        part = MIMEApplication(open(audio['path'], 'rb').read())
        part.add_header('Content-Disposition', 'attachment', filename=audio['name'])
        msg.attach(part)

    return msg


def lambda_handler(event, context):
    '''
    Main function that starts Transcription job
    '''
    try:
        # Get transcribe job details
        transcription_job_name = event['detail']['TranscriptionJobName']
        statuses = event['detail']['TranscriptionJobStatus']
        if 'COMPLETED' in statuses:
            # Check whether this transcription job is related to the Voicemail Task
            if re.compile(f'^({TRANSCRIBE_NAME_CONTAINS}).*$').match(transcription_job_name):
                message_id = transcription_job_name.lstrip(TRANSCRIBE_NAME_CONTAINS).split('-')[0]
                try:
                    # In case transcription job name was initially more than 200 characters in
                    # the first lambda it was trimmed thus we need to have more trustworthy source
                    # of the attachment name to retrieve it from S3
                    transcribe = boto3.client('transcribe')
                    attachment_filename = transcribe.get_transcription_job(
                        TranscriptionJobName=transcription_job_name
                    )['TranscriptionJob']['Media']['MediaFileUri'].split('/')[-1]
                except Exception:
                    print(f'ERROR while getting transcription job "{transcription_job_name} details. '
                          f'Please see the log below.')
                    traceback.print_exc()
                    return 0
                else:
                    # Get an old email message from S3
                    s3 = boto3.resource('s3')
                    download_eml = s3.Object(BUCKET_NAME, f'{KEY}{message_id}').download_file(
                        f'/tmp/{message_id}.eml')
                    # Get email body from EML file
                    eml_message = None
                    with open(f'/tmp/{message_id}.eml', 'rb') as f:
                        eml_message = BytesParser(policy=policy.default).parse(f)
                    email_body_object = eml_message.get_body(preferencelist=('plain'))
                    email_body = ''.join(email_body_object.get_content().splitlines(keepends=True))
                    # Get original WAV voicemail
                    download_audio = s3.Object(BUCKET_NAME, f'audio/{attachment_filename}').download_file(
                        f'/tmp/{transcription_job_name}')
                    attachment_bytes = None
                    with open(f'/tmp/{transcription_job_name}', 'rb') as f:
                        attachment_bytes = f.read()
                    # Get transcription JSON
                    download_transcription = s3.Object(BUCKET_NAME, f'{KEY}{transcription_job_name}.json').download_file(
                        f'/tmp/transcription.json')
                    transcription_data = None
                    with open('/tmp/transcription.json', 'r') as f:
                        transcription_data = f.read()
                    # Extract transcription from JSON
                    transcription = json.loads(transcription_data)['results']['transcripts'][0]['transcript']
                    # Generate Pre-signed URL to view S3 file
                    s3_client = boto3.client('s3')
                    audio_url = s3_client.generate_presigned_url(
                        ClientMethod='get_object',
                        ExpiresIn=604800,
                        Params={
                            'Bucket': BUCKET_NAME,
                            'Key': f'audio/{attachment_filename}'
                        }
                    )
                    # Get audio size in MB with two decimal
                    audio_size = round(os.path.getsize(f'/tmp/{transcription_job_name}') / 1048576, 2)
                    audiofile_message = f'is attached.\n' if audio_size <= 7.5 else f'can be accessed using the following link:\n{audio_url}.\n\n'
                    # Send an email
                    email_message = f'You have received a voicemail message:\n"{transcription}"\n\nFor your reference, the original voicemail message {audiofile_message}Also, the original email content is below.\n\n----Begin Original Email----\n\n{email_body}\n\n'
                    ses = boto3.client('ses')
                    message = form_email_message(EMAIL_SUBJECT,
                                                 SOURCE_EMAIL,
                                                 EMAIL_RECIPIENT,
                                                 email_message,
                                                 {'path': f'/tmp/{transcription_job_name}',
                                                  'name': attachment_filename} if audio_size <= 7.5 else {})
                    send_raw_email = ses.send_raw_email(
                        RawMessage={
                            'Data': message.as_string()
                        },
                        Source=SOURCE_EMAIL,
                        Destinations=[
                            EMAIL_RECIPIENT
                        ]
                    )
                    print(send_raw_email)
            else:
                print(f'ERROR Transcription job "{transcription_job_name}" is not related to this AWS Lambda. Skipping.')
                return 0
        elif 'FAILED' in statuses:
            ses = boto3.client('ses')
            send_email = ses.send_email(
                Source=SOURCE_EMAIL,
                Destination={
                    'ToAddresses': [ADMINISTRATIVE_CONTACT]
                },
                Message={
                    'Subject': {
                        'Data': f'Transcription job "{transcription_job_name}" failure',
                        'Charset': 'utf-8'
                    },
                    'Body': {
                        'Text': {
                            'Data': f'Dear contact,\n\nThis is an automatic notification about the Amazon Transcription Job failure.\n\n'
                                    f'Transcription Job Name: {transcription_job_name}',
                            'Charset': 'utf-8'
                        }
                    }
                }
            )
    except Exception:
        print('ERROR Unexpected error during the script run. Please refer to the error log below')
        traceback.print_exc()
        return 0
