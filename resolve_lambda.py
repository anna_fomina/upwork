import boto3
import socket
import json
import os

# Parameters taken from Lambda Environment Variables
WEB_ADDRESS = os.environ['WEB_ADDRESS']
BUCKET_NAME = os.environ['BUCKET_NAME']
FILE_KEY = os.environ['FILE_KEY']
ADDRESS_TAG = os.environ['ADDRESS_TAG']
CIDR_NOTATION = os.environ['CIDR_NOTATION']


def lambda_handler(event, context):
	'''
	Main function of the script
	'''
	# Resolve IP address
	ip = socket.gethostbyname(WEB_ADDRESS)
	# Get IP addresses from the file
	s3 = boto3.client('s3')
	object = s3.get_object(Bucket=BUCKET_NAME, Key=FILE_KEY)
	# Get file content from S3, decode and remove special symbols ('\r')
	content = object['Body'].read().decode('utf-8').replace(u'\r', '')
	lines = content.split(u'\n')
	result_lines = []
	tag_exists = False
	ip_changed = False
	for line in lines:
		if ADDRESS_TAG in line:
			old_address = line.split(';')[0].split('/')[0]
			if old_address != ip:
				ip_changed = True
				print(u'Changing line "{0}" in the file.'.format(line))
				if line != '\n': result_lines.append(u"{0}/{1} ; {2}\r\n".format(ip, CIDR_NOTATION, ADDRESS_TAG))
			tag_exists = True
		else:
			# Store the line for new content
			if line != '\n': result_lines.append(u"{0}\r\n".format(line))
	if not tag_exists:
		# Add the IP if an entry doesn't exist
		ip_changed = True
		line = "{0}/{1} ; {2}".format(ip, CIDR_NOTATION, ADDRESS_TAG)
		print('Adding line "{0}" to the file.'.format(line.rstrip('\n')))
		if line != '\n': result_lines.append(u"{0}\r\n".format(line))
	# Change file if IP has been changed
	if ip_changed:
		res = ''.join(result_lines)
		upload_file = s3.put_object(Bucket=BUCKET_NAME,
		                            Key=FILE_KEY,
									Body=res)
	else:
		print('IP Address remains the same. No change needed.')